import driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.BucketPage;
import pages.NavigationPage;
import pages.ProductPage;

public class RozetkaTest {
    private WebDriver driver;
    private String url = "https://rozetka.com.ua/58620082/p58620082/";
    private NavigationPage navigation;
    private ProductPage product;
    private BucketPage bucket;

    @BeforeTest
    @Parameters("browser")
    public void setupDriver(String browser) {
        DriverManager dm = new DriverManager(browser);
        this.driver = dm.getDriver();

        this.navigation = new NavigationPage(driver, url);
        this.product = navigation.getProduct();
        this.bucket = navigation.getBucket();
    };

    @Test
    public void addGoodToBucket() {
        navigation.makePurchase();
        bucket.openBucketPopup();
        Assert.assertTrue(bucket.confirmBucketHasGood());
    }

    @Test
    public void removeGoodFromBucket() {
        bucket.cancelPurchase();
        Assert.assertTrue(bucket.confirmBucketIsEmpty());
    }

    @AfterTest
    public void closeDriver() {
        driver.close();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
