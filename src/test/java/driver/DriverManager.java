package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Scanner;

public class DriverManager {
    private Browser browser;
    private WebDriver driver;

    public DriverManager(String browser) {
        switch (Browser.valueOf(browser.toUpperCase())) {
            case GECKO:
                driver = new FirefoxDriver();
                System.setProperty("webdriver.gecko.driver", System.getProperty("selenium") + "\\geckodrive.exe");
                break;
            default:
                driver = new ChromeDriver();
                System.setProperty("webdriver.chrome.driver", System.getProperty("selenium") + "\\chromedriver.exe");
                break;
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public String toString() {
        return browser.toString();
    }
}
