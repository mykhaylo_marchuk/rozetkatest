package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/*
* Entry point for all page objects
* */
public class NavigationPage {
    private WebDriver driver;
    private Actions actions;

    @FindBy(tagName = "h1")
    private WebElement nameElement;

    @FindBy(xpath = "//span[contains(@class, \"detail-buy-btn\")]//button[@class=\"btn-link-i\"]")
    private WebElement buyButton;

    private ProductPage product;
    private BucketPage bucket;

    public NavigationPage(WebDriver driver, String url) {
        driver.get(url);
        this.driver = driver;
        PageFactory.initElements(driver, this);

        actions = new Actions(this.driver);

        product = new ProductPage(driver);
        bucket = new BucketPage(driver);
    }

    private void highlightText(WebElement el) {
//        actions.moveToElement(el).clickAndHold().moveByOffset(100, 0).release().perform();

        actions.sendKeys(Keys.HOME).build().perform();
        int length = el.getText().length();

        actions.keyDown(Keys.LEFT_SHIFT);
        for (int i = 0; i < length; i++){
            actions.sendKeys(Keys.ARROW_RIGHT);
        }
        actions.keyUp(Keys.LEFT_SHIFT);
        actions.build().perform();
    }

    private void waitForElementLoading(WebElement el) {
        while (true) {
            try {
                el.findElements(By.xpath("./*"));
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                break;
            }
            catch (NoSuchElementException e) {
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            }
        }
    }

    public void makePurchase() {
        while (true) {
            try {
                // Stage before popup loading
                waitForElementLoading(nameElement);
                highlightText(nameElement);
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

                // Stage after popup loading
                waitForElementLoading(buyButton);
                buyButton.click();
                waitForElementLoading(product.getContinueShoppingButton());
                highlightText(product.getNameElementAtPopup());
                product.getContinueShoppingButton().click();
                break;
            }
            catch (StaleElementReferenceException e) {
                driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
            }
            catch (ElementClickInterceptedException e) {
                try {
                    bucket.getBucketWindowCloseElement().click();
                }
                finally {
                    continue;
                }
            }
        }
    }

    public ProductPage getProduct() {
        return product;
    }

    public void setProduct(ProductPage product) {
        this.product = product;
    }

    public BucketPage getBucket() {
        return bucket;
    }

    public void setBucket(BucketPage bucket) {
        this.bucket = bucket;
    }
}
