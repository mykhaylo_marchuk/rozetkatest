package pages;

import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class BucketPage {
    private WebDriver driver;

    @FindBy(xpath = "//li[contains(@class, \"header-actions__item_type_cart\")]//a")
    private WebElement bucketElement;

    @FindBy(xpath = "//div[@class=\"cart-remove\"]")
    private WebElement removeGood;

    @FindBy(xpath = "//a[contains(@class, \"cart-remove-popup-delete\")]")
    private WebElement confirmingRemoveGood;

    @FindBy(xpath = "//a[contains(@class, \"rz-popup-close\")]")
    private WebElement bucketWindowCloseElement;

    // For empty bucket
    @FindBy(xpath = "//div[contains(@class, \"cart-dummy-inner\")]")
    private WebElement emptyBucketHeader;

    public BucketPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openBucketPopup() {
        bucketElement.click();
    }

    public boolean confirmBucketHasGood() {
        return removeGood != null;
    }

    public void cancelPurchase() {
        while (true) {
            try {
                removeGood.click();
                confirmingRemoveGood.click();
                break;
            }
            catch (NoSuchElementException e) {
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            }
            catch (ElementClickInterceptedException e) {
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            }
        }
    }

    public boolean confirmBucketIsEmpty() {
        return emptyBucketHeader != null;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getBucketElement() {
        return bucketElement;
    }

    public void setBucketElement(WebElement bucketElement) {
        this.bucketElement = bucketElement;
    }

    public WebElement getRemoveGood() {
        return removeGood;
    }

    public void setRemoveGood(WebElement removeGood) {
        this.removeGood = removeGood;
    }

    public WebElement getConfirmingRemoveGood() {
        return confirmingRemoveGood;
    }

    public void setConfirmingRemoveGood(WebElement confirmingRemoveGood) {
        this.confirmingRemoveGood = confirmingRemoveGood;
    }

    public WebElement getBucketWindowCloseElement() {
        return bucketWindowCloseElement;
    }

    public void setBucketWindowCloseElement(WebElement bucketWindowCloseElement) {
        this.bucketWindowCloseElement = bucketWindowCloseElement;
    }

    public WebElement getEmptyBucketHeader() {
        return emptyBucketHeader;
    }

    public void setEmptyBucketHeader(WebElement emptyBucketHeader) {
        this.emptyBucketHeader = emptyBucketHeader;
    }
}
