package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage {
    private WebDriver driver;

    @FindBy(xpath = "//a[@class=\"purchase-title novisited\"]")
    private WebElement nameElementAtPopup;

    @FindBy(xpath = "//a[contains(@class, \"cart-bottom-continue\")]")
    private WebElement continueShoppingButton;

    public ProductPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getNameElementAtPopup() {
        return nameElementAtPopup;
    }

    public void setNameElementAtPopup(WebElement nameElementAtPopup) {
        this.nameElementAtPopup = nameElementAtPopup;
    }

    public WebElement getContinueShoppingButton() {
        return continueShoppingButton;
    }

    public void setContinueShoppingButton(WebElement continueShoppingButton) {
        this.continueShoppingButton = continueShoppingButton;
    }
}
